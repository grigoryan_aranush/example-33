﻿using System;
class Building
{
    public int Area;
    public int Floors;
    public int Occupants;

    public Building(int a,int f, int o)
    {
        Area = a;
        Floors = f;
        Occupants = o;
    }

    public int AreaPerPerson()
    {
        return Area / Occupants;
    }

    public int MaxOccupant(int minArea)
    {
        return Area / minArea;
    }
}
class BuildingDemo
{

    static void Main()
    {
        Building house = new Building(50, 1,4);
        Building office = new Building(2500,2,50);

        Console.WriteLine("house areaPP is " + house.AreaPerPerson() + "the maximum occupant number is " + house.MaxOccupant(10));
        Console.WriteLine("office areaPP is " + office.AreaPerPerson() + "the maximum occupant number is " + office.MaxOccupant(5));

    }

}